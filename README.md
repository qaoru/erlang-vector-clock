# erlang-vector-clock

Generates messages exchange between a specified number of processes using a vector clock for logical time synchronization.

## Usage

```
# compile veclock module
erlc veclock.erl

# run escript
escript -c main.escript <nb_procs>
```

## Description

`main.escript` calls a function `test/1` that runs 4 differents scenarii :

* basic (each process sends a message to all siblings)
* basic + random local events added
* basic + some process sends more messages
* complete

Each scenario follows the same steps:

* Processes generation
* Events scheduling
* Root process terminates all the generated processes

Tests can be disabled by commenting/removing their call in `veclock:test/1`.