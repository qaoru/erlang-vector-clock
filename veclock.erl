-module(veclock).
-export([
  loop/2, 
  build_register/1, build/1,
  print/2, print/3, 
  set_nth/3, increment/1,
  schedule_snd_random/2,
  schedule_snd/2,
  gather/2,
  nb_loc/0, nb_snd/0,
  max_schedule_delay_ms/0,
  schedule_loc_n/3,
  schedule_snd_n/3,
  test/1
]).

-record(state, {
  id,
  clock,
  init=true
}).

%%%%%%%%%%%%%%%%%%%% Configuration %%%%%%%%%%%%%%%%%%%%

% Max delay for send_after
max_schedule_delay_ms() -> 500.

% Number of local events to trigger (for test scenario)
nb_loc() -> 6.

% Number of additionnal send events to trigger (for test scenario)
nb_snd() -> 6.

%%%%%%%%%%%%%%%%%%%% Print functions %%%%%%%%%%%%%%%%%%%%

print(#state{id=Id, clock=Clock}, Str) ->
  io:format("~p ~w > " ++ Str, [Id, Clock]).

print(#state{id=Id, clock=Clock}, Fmt, Args) ->
  io:format("~p ~w > " ++ Fmt, [Id | [Clock | Args]]).

%%%%%%%%%%%%%%%%%%%% Utilities %%%%%%%%%%%%%%%%%%%%

% Set nth element of a list
set_nth(1, Val, [_|T]) -> [Val|T];
set_nth(N, Val, [H|T]) -> [H|set_nth(N-1, Val, T)].

gather(0, _) -> done;
gather(N, MsgType) when N > 0 ->
  receive
    MsgType -> gather(N-1, MsgType)
  end.

% Returns a new state record with the clock value at id incremented
increment(S = #state{id=Id, clock=Clock}) when is_list(Clock), is_integer(Id) ->
  S#state{clock=set_nth(Id, lists:nth(Id, Clock) + 1, Clock)}.

update_clock(S = #state{clock=Current}, Received) when is_list(Current), is_list(Received) ->
  S#state{clock=lists:map(
    fun ({A, B}) ->
      max(A, B)
    end,
    lists:zip(Current, Received)
  )}.


%%%%%%%%%%%%%%%%%%%% Event scheduling %%%%%%%%%%%%%%%%%%%%

schedule_snd_random(To, Targets) when is_list(Targets) ->
  lists:foreach(
    fun (K) ->
      timer:send_after(rand:uniform(max_schedule_delay_ms()), To, { snd, K })
    end,
    Targets
  ).

schedule_snd(To, Targets) when is_list(Targets) ->
  lists:foreach(
    fun (K) ->
      To ! { snd, K }
    end,
    Targets
  ).

% Schedule K local events on a process list PList of size N.
schedule_loc_n(0, _, _) -> done;
schedule_loc_n(K, PList, N) ->
  Target = lists:nth(rand:uniform(N), PList),
  timer:send_after(rand:uniform(max_schedule_delay_ms()), Target, loc),
  schedule_loc_n(K-1, PList, N).

% Schedule K send events on a process list PList of size N.
% Sender and Target are random.
% Message might be ignored if Sender == Target.
schedule_snd_n(0, _, _) -> done;
schedule_snd_n(K, PList, N) ->
  From = lists:nth(rand:uniform(N), PList),
  To = lists:nth(rand:uniform(N), PList),
  timer:send_after(rand:uniform(max_schedule_delay_ms()), From, { snd, To }),
  schedule_snd_n(K-1, PList, N).


%%%%%%%%%%%%%%%%%%%% Main loop for child processes %%%%%%%%%%%%%%%%%%%%

% Child process loop.
% Each time it receives a message, the process number and its clock BEFORE
% the message parsing are printed at the beginning of the line.
loop(N, S) when S#state.init == true ->
  print(S, "Process ~p initialized. ~n", [self()]),
  loop(N, S#state{init=false});
loop(N, S) ->
  receive
    % Message reception
    { msg, SenderPID, VecStamp } ->
      % Upon reception, save own clock value with increment
      Tmp = lists:nth(S#state.id, S#state.clock) + 1,

      % Update clock
      Tmp_S = update_clock(S, VecStamp),

      % Set correct value for owd id
      NS = Tmp_S#state{clock=set_nth(S#state.id, Tmp, Tmp_S#state.clock)},
      
      print(S, "Received msg from ~p (stamp: ~w) ! New clock = ~w ~n", [SenderPID, VecStamp, NS#state.clock]),
      loop(N, NS);

    % Send command triggered by root process. Ingored if it targets itself.
    { snd, Target } when Target /= self() ->
      % Store new clock value for process id
      NS = increment(S),

      print(S, "Sending message to ~w. New clock = ~w ~n", [Target, NS#state.clock]),
      % Send message to Target with our clock as stamp
      Target ! { msg, self(), NS#state.clock },
      loop(N, NS);

    % Local event command
    loc->
      % Increment process slot in the clock
      NS = increment(S),
      print(S, "Local event triggered ! New clock = ~w ~n", [NS#state.clock]),
      loop(N, NS);

    % Stop command
    stop ->
      print(S, "Terminating.~n"),
      master ! finish,
      done
  end.

%%%%%%%%%%%%%%%%%%%% Build functions %%%%%%%%%%%%%%%%%%%%

% Spawns N processes and registers them under the atoms p1..pN.
% Returns a list of the processes pid
build_register(N) ->
  io:format("Building ~w.~n", [N]),
  % List of atoms to register
  PidToGen = [list_to_atom("p" ++ integer_to_list(X))|| X <- lists:seq(1,N)],
  % Spawn and register each process using foldr to maintain a counter for their ID
  lists:foldl(
    fun (P, Cnt) ->
      register(P, spawn(?MODULE, loop, [N, #state{id=Cnt, clock=lists:duplicate(N, 0)}, N-1])),
      Cnt+1
    end,
    1,
    PidToGen
  ),
  io:format("~w generated. ~n", [PidToGen]),
  % Returns a list of pid
  lists:map(fun (A) -> whereis(A) end, PidToGen).

% Spawns N processes and returns a list of PIDs.
build(N) ->
  io:format("Building ~w.~n", [N]),
  % Spawns N processes and store their pid in an accumulator
  Pids = lists:foldl(
    fun (P, Gen) ->
      [spawn(?MODULE, loop, [N, #state{id=P, clock=lists:duplicate(N, 0)}]) | Gen]
    end,
    [],
    lists:seq(1,N)
  ),  % The list of pids is built in reverse order, so returns the reversed list.
  lists:reverse(Pids).


%%%%%%%%%%%%%%%%%%%% Test and scenarii %%%%%%%%%%%%%%%%%%%%

test_loop(Name, N, Scenario) -> 
  io:format("Test : ~p -> Building ~p processes...~n", [Name, N]),
  Generated = build(N),
  io:format("~p processes built. PIDs (from 1st to Nth): ~w~n", [N, Generated]),
  Scenario(Generated, N),
  % Sleeping 1.5 times the maximum delay
  timer:sleep(round(max_schedule_delay_ms() * 1.5)),
  % Gathering all processes
  lists:foreach(fun (P) -> P ! stop end, Generated),
  gather(N, finish).

% Test function, requires an int as argument
% We will test multiple scenarii :
% - basic (each process sends a message to all siblings)
% - basic + random local events added
% - basic + some process sends more messages
% - complete
test(N) when is_integer(N) ->
  register(master, self()),
  io:format("Starting tests !~n~n"),
  io:format("~n##########################################################~n~n"),
  test_loop("Basic test", N,fun  basic_scenario/2),
  io:format("~n##########################################################~n~n"),
  test_loop("Loc test", N, fun basic_loc_scenario/2),
  io:format("~n##########################################################~n~n"),
  test_loop("More send test", N, fun more_snd_scenario/2),
  io:format("~n##########################################################~n~n"),
  test_loop("Complete test", N, fun complete_scenario/2),
  io:format("~n##########################################################~n~n"),
  io:format("~nAll tests completed.~n").

% Basic scenario : each process from PList sends a message to the others.
% To make it simple, it also schedules a message to be sended to itself,
% but it is ignored by the guard in the process loop.
basic_scenario(PList, _) when is_list(PList) ->
  lists:foreach(
      fun (P) ->
        schedule_snd_random(P, PList)
      end,
      PList
  ).

% Basic scenario with local events : K random local events triggered as well.
basic_loc_scenario(PList, N) when is_list(PList) ->
  % First, we setup the basic scenario
  basic_scenario(PList, N),
  % Then, we schedule random local events
  schedule_loc_n(nb_loc(), PList, N).

% More events scenario
more_snd_scenario(PList, N) when is_list(PList) ->
  % First, we setup the basic scenario
  basic_scenario(PList, N),
  % Then, we schedule more send events
  schedule_snd_n(nb_snd(), PList, N).

% Complete scenario
complete_scenario(PList, N) when is_list(PList) ->
  % First, we setup the basic scenario
  basic_scenario(PList, N),
  % Then, we schedule local events
  schedule_loc_n(nb_loc(), PList, N),
  % Finally, we schedule more send events
  schedule_snd_n(nb_snd(), PList, N).
